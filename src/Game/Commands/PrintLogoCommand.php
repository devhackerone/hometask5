<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\CommandInterface;

class PrintLogoCommand implements CommandInterface
{
    public function execute(Writer $writer, Reader $reader = null, String $parameter = null)
    {
        $writer->writeln(" .______    __  .______          ___   .___________. _______     _______.");
        $writer->writeln(" |   _  \  |  | |   _  \        /   \  |           ||   ____|   /       |");
        $writer->writeln(" |  |_)  | |  | |  |_)  |      /  ^  \ `---|  |----`|  |__     |   (----`");
        $writer->writeln(" |   ___/  |  | |      /      /  /_\  \    |  |     |   __|     \   \    ");
        $writer->writeln(" |  |      |  | |  |\  \----./  _____  \   |  |     |  |____.----)   |   ");
        $writer->writeln(" | _|      |__| | _| `._____/__/     \__\  |__|     |_______|_______/    ");
        $writer->writeln("                                                                         ");
        $writer->writeln("               ____        __  __  __        _____ __    _               ");
        $writer->writeln("              / __ )____ _/ /_/ /_/ /__     / ___// /_  (_)___           ");
        $writer->writeln("             / __  / __ `/ __/ __/ / _ \    \__ \/ __ \/ / __ \          ");
        $writer->writeln("            / /_/ / /_/ / /_/ /_/ /  __/   ___/ / / / / / /_/ /          ");
        $writer->writeln("           /_____/\__,_/\__/\__/_/\___/   /____/_/ /_/_/ .___/           ");
        $writer->writeln("                                                      /_/                ");
        $writer->writeln("                                                                         ");

    }
}