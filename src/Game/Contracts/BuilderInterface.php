<?php

namespace BinaryStudioAcademy\Game\Contracts;


interface BuilderInterface
{
    public function getType(): string;

    public function setType(string $type): void;

    public function getPositionNumber(): int;

    public function setPositionNumber(int $positionNumber): void;

    public function getPosition(): string;

    public function setPosition(string $position): void;

    public function setName(string $name): void;

    public function setStrength(int $strength): void;

    public function setArmour(int $armour): void;

    public function setLuck(int $luck): void;

    public function setHealth(int $health): void;

    public function setHold(array $hold): void;

    public function getName(): string;

    public function getStrength(): int;

    public function getArmour(): int;

    public function getLuck(): int;

    public function getHealth(): int;

    public function getHold(): array;

    public function fire(Random $random, Math $math, BuilderShip $enemyShip): int;

    public function died(): void;

}