<?php


namespace BinaryStudioAcademy\Game\Contracts;


interface FactoryInterface
{
    public static function getInstance() : FactoryInterface ;

    //public function getHarborNumber(Int $resourceName) : int ;
}