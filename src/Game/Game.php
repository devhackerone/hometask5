<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Commands\PrintLogoCommand;

class Game
{
    private $random;

    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    public function start(Reader $reader, Writer $writer)
    {
        $logoCommand = new PrintLogoCommand();
        $logoCommand->execute($writer);
        $writer->writeln('Task of the game is to drown the HMS Royal Sovereign!');
        $writer->writeln('Press enter to start... ');
        $input = trim($reader->read());
        $writer->writeln('Adventure has begun. Wish you good luck!');
        $writer->writeln('Type "help" to display the list of commands.');

        CommandProcessor::executeLoop($writer, $reader);
    }

    public function run(Reader $reader, Writer $writer)
    {
        CommandProcessor::execute($writer, $reader);
    }
}
