<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\BuilderShips\BuilderShip;

class Harbor
{
    private $harborNumber;
    private $title;
    private $ship;

    public function __construct(BuilderShip $ship, string $title, int $harborNumber)
    {
        $this->ship = $ship;
        $this->title = $title;
        $this->harborNumber = $harborNumber;
    }

    public function getShip(): BuilderShip
    {
        return $this->ship;
    }

    public function setShip(BuilderShip $ship): void
    {
        $this->ship = $ship;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getHarborNumber(): int
    {
        return $this->harborNumber;
    }
}