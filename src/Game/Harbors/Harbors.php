<?php


namespace BinaryStudioAcademy\Game\Harbors;


use BinaryStudioAcademy\Game\BuilderShips\BuilderTypeShips;

class Harbors
{

    public static function createHarbours(): array
    {
        return [
            'Pirates Harbor' => new Harbor(BuilderTypeShips::createShip('pirate'), 'Pirates Hardbor', 1),
            'Southhampton' => new Harbor(BuilderTypeShips::createShip('schooner'), 'Southhampton', 2),
            'Fishguard' => new Harbor(BuilderTypeShips::createShip('schooner'), 'Fishguard', 3),
            'Salt End' => new Harbor(BuilderTypeShips::createShip('schooner'), 'Salt End', 4),
            'Isle of Grain' => new Harbor(BuilderTypeShips::createShip('schooner'), 'Isle of Grain', 5),
            'Grays' => new Harbor(BuilderTypeShips::createShip('battle'), 'Grays', 6),
            'Felixstowe' => new Harbor(BuilderTypeShips::createShip('battle'), 'Felixstowe', 7),
            'London Docks' => new Harbor(BuilderTypeShips::createShip('royal'), 'London Docks', 8),
        ];
    }
}