<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\CommandInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class CommandProcessor
{
    public static function executeLoop(Writer $writer, Reader $reader = null){

        while (!isset($command) || $command <> 'exit') {
            $input = trim($reader->read());

            if ($input == 'exit') break;

            list($command, $parameter) = array_pad(explode(" ", $input),2,null);

            $command = str_replace("-", "", $command);

            $classCommand = "BinaryStudioAcademy\\Game\\Commands\\".ucfirst($command)."Command";

            if (class_exists($classCommand)) {
                $comm = new $classCommand();
                $comm->execute($writer, $reader, $parameter);
            } else {
                $writer->writeln( "Wrong command, use 'help' command to get list of allowed commands");
            }
        }
    }

    public static function execute(Writer $writer, Reader $reader = null){

        $input = trim($reader->read());

        list($command, $parameter) = array_pad(explode(":", $input),2,null);

        $classCommand = "BinaryStudioAcademy\\Game\\Commands\\".ucfirst($command)."Command";

        if (class_exists($classCommand)) {
            $comm = new $classCommand();
            $comm->execute($writer, $reader, $parameter);
        } else {
            $writer->writeln( "Command '$command' not found");
        }
    }

}