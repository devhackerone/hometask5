<?php

namespace BinaryStudioAcademy\Game\BuilderShips;

class BuilderTypeShips
{
    public static function builderTypeShip(string $type): BuilderAddShip
    {
        switch ($type) {
            case 'player':
                return (new BuilderAddShip)
                ->addName('Pirates')
                ->addStrength(4)
                ->addArmour(4)
                ->addLuck(4)
                ->addHealth(60)
                ->addHold(['_', '_', '_'])
                ->addPosition('Pirates Harbor')
                ->addType('pirate')
                ->getShip();

            case 'schooner':
                return (new BuilderAddShip)
                ->addName('Royal Patrool Schooner')
                ->addStrength(3)
                ->addArmour(3)
                ->addLuck(2)
                ->addHealth(50)
                ->addHold(['gold', '_', '_'])
                ->addType('schooner')
                ->getShip();

            case 'battle':
                return (new BuilderAddShip)
                ->addName('Royal Battle Ship')
                ->addStrength(6)
                ->addArmour(6)
                ->addLuck(5)
                ->addHealth(80)
                ->addHold(['rum', '_', '_'])
                ->addType('battle')
                ->getShip();

            case 'royal':
                return (new BuilderAddShip)
                ->addName('HMS Royal Sovereign')
                ->addStrength(10)
                ->addArmour(10)
                ->addLuck(10)
                ->addHealth(100)
                ->addHold(['gold', 'gold', 'rum'])
                ->addType('royal')
                ->getShip();
        }
    }
}