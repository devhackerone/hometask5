<?php

namespace BinaryStudioAcademy\Game\BuilderShips;

class BuilderAddShip
{
    private $ship;

    public function __construct()
    {
        $this->ship = new BuilderShip();
    }

    public function addStrength(int $strength): self
    {
        return $this->ship->setStrength($strength);
    }

    public function addName(string $name): self
    {
        return $this->ship->setName($name);
    }

    public function addArmour(int $armour): self
    {
        return $this->ship->setArmour($armour);
    }

    public function addLuck(int $luck): self
    {
        return $this->ship->setLuck($luck);
    }

    public function addHealth(int $health): self
    {
        return $this->ship->setHealth($health);
    }

    public function addHold(array $hold): self
    {
        return $this->ship->setHold($hold);
    }

    public function addPosition(string $position): self
    {
        return $this->ship->setPosition($position);
    }

    public function addPositionNumber(int $positionNumber): self
    {

        return $this->ship->setPositionNumber($positionNumber);
    }

    public function addType(string $type): self
    {
        return $this->ship->setType($type);
    }

    public function getShip(): BuilderShip
    {
        return $this->ship;
    }

}