<?php

namespace BinaryStudioAcademy\Game\BuilderShips;

use BinaryStudioAcademy\Game\Contracts\BuilderInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\Math;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class BuilderShip implements BuilderInterface
{
    private $name;
    private $strength;
    private $armour;
    private $luck;
    private $health;
    private $hold;
    private $position;
    private $positionNumber;
    private $type;

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getPositionNumber(): int
    {
        return $this->positionNumber;
    }

    public function setPositionNumber(int $positionNumber): void
    {
        $this->positionNumber = $positionNumber;
    }

    public function getPosition(): string
    {
        return $this->position;
    }

    public function setPosition(string $position): void
    {
        $this->position = $position;
    }


    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setStrength(int $strength): void
    {
        $this->strength = $strength;
    }

    public function setArmour(int $armour): void
    {
        $this->armour = $armour;
    }

    public function setLuck(int $luck): void
    {
        $this->luck = $luck;
    }

    public function setHealth(int $health): void
    {
        $this->health = $health;
    }

    public function setHold(array $hold): void
    {
        $this->hold = $hold;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getArmour(): int
    {
        return $this->armour;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

    public function fire(Random $random, Math $math, BuilderShip $enemyShip): int
    {
        $luck = $math->luck($random, $this->getLuck());
        if ($luck) {
            $damage = $math->damage($this->getStrength(), $enemyShip->getArmour());
            $enemyShip->setHealth($enemyShip->getHealth() - $damage);
            return $damage;
        }
        return 0;
    }

    public function died(): void
    {
        $this->setHealth(60);
        $this->setHold([]);
        $this->subStrength();
        $this->subArmoure();
        $this->subLuck();
        $this->setPositionNumber(1);
        $this->setPosition('Pirates Harbor');
    }

    private function subStrength(): void
    {
        $this->strength--;
    }

    private function subArmoure(): void
    {
        $this->armour--;
    }

    private function subLuck(): void
    {
        $this->luck--;
    }

}